﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SMB.Models;

namespace SMB.Controllers
{
    public class ITEM_LISTController : Controller
    {
        private ShopEntities db = new ShopEntities();

        // GET: ITEM_LIST
        public ActionResult Index()
        {
            var iTEM_LIST = db.ITEM_LIST.Include(i => i.ITEM_FACTOR).Include(i => i.ITEM_GROUP1).Include(i => i.TYPE_ITEM).Include(i => i.ITEM_UOM1);
            return View(iTEM_LIST.ToList());
        }

        // GET: ITEM_LIST/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ITEM_LIST iTEM_LIST = db.ITEM_LIST.Find(id);
            if (iTEM_LIST == null)
            {
                return HttpNotFound();
            }
            return View(iTEM_LIST);
        }

        // GET: ITEM_LIST/Create
        public ActionResult Create()
        {
            ViewBag.ITEM_FAC = new SelectList(db.ITEM_FACTOR, "FAC_CODE", "FAC_NAME");
            ViewBag.ITEM_GROUP = new SelectList(db.ITEM_GROUP, "GROUP_CODE", "GROUP_NAME");
            ViewBag.ITEM_TYPE = new SelectList(db.TYPE_ITEM, "TYPE_CODE", "TYPE_NAME");
            ViewBag.ITEM_UOM = new SelectList(db.ITEM_UOM, "UOM_CODE", "UOM_NAME");
            return View();
        }

        // POST: ITEM_LIST/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ITEM_CODE,ITEM_NAME,ITEM_DESC,ITEM_DP,ITEM_TP,ITEM_SP,ITEM_MP,ITEM_TYPE,ITEM_GROUP,ITEM_UOM,ITEM_FAC")] ITEM_LIST iTEM_LIST)
        {
            if (ModelState.IsValid)
            {
                db.ITEM_LIST.Add(iTEM_LIST);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ITEM_FAC = new SelectList(db.ITEM_FACTOR, "FAC_CODE", "FAC_NAME", iTEM_LIST.ITEM_FAC);
            ViewBag.ITEM_GROUP = new SelectList(db.ITEM_GROUP, "GROUP_CODE", "GROUP_NAME", iTEM_LIST.ITEM_GROUP);
            ViewBag.ITEM_TYPE = new SelectList(db.TYPE_ITEM, "TYPE_CODE", "TYPE_NAME", iTEM_LIST.ITEM_TYPE);
            ViewBag.ITEM_UOM = new SelectList(db.ITEM_UOM, "UOM_CODE", "UOM_NAME", iTEM_LIST.ITEM_UOM);
            return View(iTEM_LIST);
        }

        // GET: ITEM_LIST/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ITEM_LIST iTEM_LIST = db.ITEM_LIST.Find(id);
            if (iTEM_LIST == null)
            {
                return HttpNotFound();
            }
            ViewBag.ITEM_FAC = new SelectList(db.ITEM_FACTOR, "FAC_CODE", "FAC_NAME", iTEM_LIST.ITEM_FAC);
            ViewBag.ITEM_GROUP = new SelectList(db.ITEM_GROUP, "GROUP_CODE", "GROUP_NAME", iTEM_LIST.ITEM_GROUP);
            ViewBag.ITEM_TYPE = new SelectList(db.TYPE_ITEM, "TYPE_CODE", "TYPE_NAME", iTEM_LIST.ITEM_TYPE);
            ViewBag.ITEM_UOM = new SelectList(db.ITEM_UOM, "UOM_CODE", "UOM_NAME", iTEM_LIST.ITEM_UOM);
            return View(iTEM_LIST);
        }

        // POST: ITEM_LIST/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ITEM_CODE,ITEM_NAME,ITEM_DESC,ITEM_DP,ITEM_TP,ITEM_SP,ITEM_MP,ITEM_TYPE,ITEM_GROUP,ITEM_UOM,ITEM_FAC")] ITEM_LIST iTEM_LIST)
        {
            if (ModelState.IsValid)
            {
                db.Entry(iTEM_LIST).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ITEM_FAC = new SelectList(db.ITEM_FACTOR, "FAC_CODE", "FAC_NAME", iTEM_LIST.ITEM_FAC);
            ViewBag.ITEM_GROUP = new SelectList(db.ITEM_GROUP, "GROUP_CODE", "GROUP_NAME", iTEM_LIST.ITEM_GROUP);
            ViewBag.ITEM_TYPE = new SelectList(db.TYPE_ITEM, "TYPE_CODE", "TYPE_NAME", iTEM_LIST.ITEM_TYPE);
            ViewBag.ITEM_UOM = new SelectList(db.ITEM_UOM, "UOM_CODE", "UOM_NAME", iTEM_LIST.ITEM_UOM);
            return View(iTEM_LIST);
        }

        // GET: ITEM_LIST/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ITEM_LIST iTEM_LIST = db.ITEM_LIST.Find(id);
            if (iTEM_LIST == null)
            {
                return HttpNotFound();
            }
            return View(iTEM_LIST);
        }

        // POST: ITEM_LIST/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            ITEM_LIST iTEM_LIST = db.ITEM_LIST.Find(id);
            db.ITEM_LIST.Remove(iTEM_LIST);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
