﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SMB.Models;

namespace SMB.Controllers
{
    public class TRN_DETAILController : Controller
    {
        private ShopEntities db = new ShopEntities();

        // GET: TRN_DETAIL
        public ActionResult Index()
        {
            return View(db.TRN_DETAIL.ToList());
        }

        // GET: TRN_DETAIL/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TRN_DETAIL tRN_DETAIL = db.TRN_DETAIL.Find(id);
            if (tRN_DETAIL == null)
            {
                return HttpNotFound();
            }
            return View(tRN_DETAIL);
        }

        // GET: TRN_DETAIL/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TRN_DETAIL/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TRD_NO,TRD_TRM,TRD_LINE,TRD_ITEM,TRD_PQTY,TRD_PRATE,TRD_SQTY,TRD_SRATE,TRD_FAC,TRD_ORD_NO,TRD_ORD_LINE")] TRN_DETAIL tRN_DETAIL)
        {
            if (ModelState.IsValid)
            {
                db.TRN_DETAIL.Add(tRN_DETAIL);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tRN_DETAIL);
        }

        // GET: TRN_DETAIL/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TRN_DETAIL tRN_DETAIL = db.TRN_DETAIL.Find(id);
            if (tRN_DETAIL == null)
            {
                return HttpNotFound();
            }
            return View(tRN_DETAIL);
        }

        // POST: TRN_DETAIL/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TRD_NO,TRD_TRM,TRD_LINE,TRD_ITEM,TRD_PQTY,TRD_PRATE,TRD_SQTY,TRD_SRATE,TRD_FAC,TRD_ORD_NO,TRD_ORD_LINE")] TRN_DETAIL tRN_DETAIL)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tRN_DETAIL).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tRN_DETAIL);
        }

        // GET: TRN_DETAIL/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TRN_DETAIL tRN_DETAIL = db.TRN_DETAIL.Find(id);
            if (tRN_DETAIL == null)
            {
                return HttpNotFound();
            }
            return View(tRN_DETAIL);
        }

        // POST: TRN_DETAIL/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            TRN_DETAIL tRN_DETAIL = db.TRN_DETAIL.Find(id);
            db.TRN_DETAIL.Remove(tRN_DETAIL);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
