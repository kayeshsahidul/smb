﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SMB.Models;

namespace SMB.Controllers
{
    public class TYPE_TRNController : Controller
    {
        private ShopEntities db = new ShopEntities();

        // GET: TYPE_TRN
        public ActionResult Index()
        {
            return View(db.TYPE_TRN.ToList());
        }

        // GET: TYPE_TRN/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TYPE_TRN tYPE_TRN = db.TYPE_TRN.Find(id);
            if (tYPE_TRN == null)
            {
                return HttpNotFound();
            }
            return View(tYPE_TRN);
        }

        // GET: TYPE_TRN/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TYPE_TRN/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TRN_CODE,TRN_NAME,TRN_DESC,TRN_SIGN")] TYPE_TRN tYPE_TRN)
        {
            if (ModelState.IsValid)
            {
                db.TYPE_TRN.Add(tYPE_TRN);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tYPE_TRN);
        }

        // GET: TYPE_TRN/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TYPE_TRN tYPE_TRN = db.TYPE_TRN.Find(id);
            if (tYPE_TRN == null)
            {
                return HttpNotFound();
            }
            return View(tYPE_TRN);
        }

        // POST: TYPE_TRN/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TRN_CODE,TRN_NAME,TRN_DESC,TRN_SIGN")] TYPE_TRN tYPE_TRN)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tYPE_TRN).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tYPE_TRN);
        }

        // GET: TYPE_TRN/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TYPE_TRN tYPE_TRN = db.TYPE_TRN.Find(id);
            if (tYPE_TRN == null)
            {
                return HttpNotFound();
            }
            return View(tYPE_TRN);
        }

        // POST: TYPE_TRN/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            TYPE_TRN tYPE_TRN = db.TYPE_TRN.Find(id);
            db.TYPE_TRN.Remove(tYPE_TRN);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
