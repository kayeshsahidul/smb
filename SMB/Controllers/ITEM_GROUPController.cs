﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SMB.Models;

namespace SMB.Controllers
{
    public class ITEM_GROUPController : Controller
    {
        private ShopEntities db = new ShopEntities();

        // GET: ITEM_GROUP
        public ActionResult Index()
        {
            return View(db.ITEM_GROUP.ToList());
        }

        // GET: ITEM_GROUP/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ITEM_GROUP iTEM_GROUP = db.ITEM_GROUP.Find(id);
            if (iTEM_GROUP == null)
            {
                return HttpNotFound();
            }
            return View(iTEM_GROUP);
        }

        // GET: ITEM_GROUP/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ITEM_GROUP/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "GROUP_CODE,GROUP_NAME")] ITEM_GROUP iTEM_GROUP)
        {
            if (ModelState.IsValid)
            {
                db.ITEM_GROUP.Add(iTEM_GROUP);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(iTEM_GROUP);
        }

        // GET: ITEM_GROUP/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ITEM_GROUP iTEM_GROUP = db.ITEM_GROUP.Find(id);
            if (iTEM_GROUP == null)
            {
                return HttpNotFound();
            }
            return View(iTEM_GROUP);
        }

        // POST: ITEM_GROUP/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "GROUP_CODE,GROUP_NAME")] ITEM_GROUP iTEM_GROUP)
        {
            if (ModelState.IsValid)
            {
                db.Entry(iTEM_GROUP).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(iTEM_GROUP);
        }

        // GET: ITEM_GROUP/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ITEM_GROUP iTEM_GROUP = db.ITEM_GROUP.Find(id);
            if (iTEM_GROUP == null)
            {
                return HttpNotFound();
            }
            return View(iTEM_GROUP);
        }

        // POST: ITEM_GROUP/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            ITEM_GROUP iTEM_GROUP = db.ITEM_GROUP.Find(id);
            db.ITEM_GROUP.Remove(iTEM_GROUP);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
