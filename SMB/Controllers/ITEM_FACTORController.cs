﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SMB.Models;

namespace SMB.Controllers
{
    public class ITEM_FACTORController : Controller
    {
        private ShopEntities db = new ShopEntities();

        // GET: ITEM_FACTOR
        public ActionResult Index()
        {
            return View(db.ITEM_FACTOR.ToList());
        }

        // GET: ITEM_FACTOR/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ITEM_FACTOR iTEM_FACTOR = db.ITEM_FACTOR.Find(id);
            if (iTEM_FACTOR == null)
            {
                return HttpNotFound();
            }
            return View(iTEM_FACTOR);
        }

        // GET: ITEM_FACTOR/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ITEM_FACTOR/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FAC_CODE,FAC_NAME,FAC_CNV")] ITEM_FACTOR iTEM_FACTOR)
        {
            if (ModelState.IsValid)
            {
                db.ITEM_FACTOR.Add(iTEM_FACTOR);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(iTEM_FACTOR);
        }

        // GET: ITEM_FACTOR/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ITEM_FACTOR iTEM_FACTOR = db.ITEM_FACTOR.Find(id);
            if (iTEM_FACTOR == null)
            {
                return HttpNotFound();
            }
            return View(iTEM_FACTOR);
        }

        // POST: ITEM_FACTOR/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "FAC_CODE,FAC_NAME,FAC_CNV")] ITEM_FACTOR iTEM_FACTOR)
        {
            if (ModelState.IsValid)
            {
                db.Entry(iTEM_FACTOR).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(iTEM_FACTOR);
        }

        // GET: ITEM_FACTOR/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ITEM_FACTOR iTEM_FACTOR = db.ITEM_FACTOR.Find(id);
            if (iTEM_FACTOR == null)
            {
                return HttpNotFound();
            }
            return View(iTEM_FACTOR);
        }

        // POST: ITEM_FACTOR/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            ITEM_FACTOR iTEM_FACTOR = db.ITEM_FACTOR.Find(id);
            db.ITEM_FACTOR.Remove(iTEM_FACTOR);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
