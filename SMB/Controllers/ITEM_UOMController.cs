﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SMB.Models;

namespace SMB.Controllers
{
    public class ITEM_UOMController : Controller
    {
        private ShopEntities db = new ShopEntities();

        // GET: ITEM_UOM
        public ActionResult Index()
        {
            return View(db.ITEM_UOM.ToList());
        }

        // GET: ITEM_UOM/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ITEM_UOM iTEM_UOM = db.ITEM_UOM.Find(id);
            if (iTEM_UOM == null)
            {
                return HttpNotFound();
            }
            return View(iTEM_UOM);
        }

        // GET: ITEM_UOM/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ITEM_UOM/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UOM_CODE,UOM_NAME")] ITEM_UOM iTEM_UOM)
        {
            if (ModelState.IsValid)
            {
                db.ITEM_UOM.Add(iTEM_UOM);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(iTEM_UOM);
        }

        // GET: ITEM_UOM/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ITEM_UOM iTEM_UOM = db.ITEM_UOM.Find(id);
            if (iTEM_UOM == null)
            {
                return HttpNotFound();
            }
            return View(iTEM_UOM);
        }

        // POST: ITEM_UOM/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UOM_CODE,UOM_NAME")] ITEM_UOM iTEM_UOM)
        {
            if (ModelState.IsValid)
            {
                db.Entry(iTEM_UOM).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(iTEM_UOM);
        }

        // GET: ITEM_UOM/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ITEM_UOM iTEM_UOM = db.ITEM_UOM.Find(id);
            if (iTEM_UOM == null)
            {
                return HttpNotFound();
            }
            return View(iTEM_UOM);
        }

        // POST: ITEM_UOM/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            ITEM_UOM iTEM_UOM = db.ITEM_UOM.Find(id);
            db.ITEM_UOM.Remove(iTEM_UOM);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
