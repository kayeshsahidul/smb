﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SMB.Models;

namespace SMB.Controllers
{
    public class CUSTOMERController : Controller
    {
        private ShopEntities db = new ShopEntities();

        // GET: CUSTOMER
        public ActionResult Index()
        {
            var cUSTOMERS = db.CUSTOMERS.Include(c => c.TYPE_CUST);
            return View(cUSTOMERS.ToList());
        }

        // GET: CUSTOMER/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CUSTOMER cUSTOMER = db.CUSTOMERS.Find(id);
            if (cUSTOMER == null)
            {
                return HttpNotFound();
            }
            return View(cUSTOMER);
        }

        // GET: CUSTOMER/Create
        public ActionResult Create()
        {
            ViewBag.CUST_TYPE = new SelectList(db.TYPE_CUST, "TYPE_CODE", "TYPE_NAME");
            return View();
        }

        // POST: CUSTOMER/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CUST_ID,CUST_NAME,CUST_MOBILE,CUST_EMAIL,CUST_ADDR1,CUST_ADDR2,CUST_TYPE,CUST_AREA")] CUSTOMER cUSTOMER)
        {
            if (ModelState.IsValid)
            {
                db.CUSTOMERS.Add(cUSTOMER);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CUST_TYPE = new SelectList(db.TYPE_CUST, "TYPE_CODE", "TYPE_NAME", cUSTOMER.CUST_TYPE);
            return View(cUSTOMER);
        }

        // GET: CUSTOMER/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CUSTOMER cUSTOMER = db.CUSTOMERS.Find(id);
            if (cUSTOMER == null)
            {
                return HttpNotFound();
            }
            ViewBag.CUST_TYPE = new SelectList(db.TYPE_CUST, "TYPE_CODE", "TYPE_NAME", cUSTOMER.CUST_TYPE);
            return View(cUSTOMER);
        }

        // POST: CUSTOMER/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CUST_ID,CUST_NAME,CUST_MOBILE,CUST_EMAIL,CUST_ADDR1,CUST_ADDR2,CUST_TYPE,CUST_AREA")] CUSTOMER cUSTOMER)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cUSTOMER).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CUST_TYPE = new SelectList(db.TYPE_CUST, "TYPE_CODE", "TYPE_NAME", cUSTOMER.CUST_TYPE);
            return View(cUSTOMER);
        }

        // GET: CUSTOMER/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CUSTOMER cUSTOMER = db.CUSTOMERS.Find(id);
            if (cUSTOMER == null)
            {
                return HttpNotFound();
            }
            return View(cUSTOMER);
        }

        // POST: CUSTOMER/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            CUSTOMER cUSTOMER = db.CUSTOMERS.Find(id);
            db.CUSTOMERS.Remove(cUSTOMER);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
