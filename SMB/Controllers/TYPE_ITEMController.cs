﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SMB.Models;

namespace SMB.Controllers
{
    public class TYPE_ITEMController : Controller
    {
        private ShopEntities db = new ShopEntities();

        // GET: TYPE_ITEM
        public ActionResult Index()
        {
            return View(db.TYPE_ITEM.ToList());
        }

        // GET: TYPE_ITEM/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TYPE_ITEM tYPE_ITEM = db.TYPE_ITEM.Find(id);
            if (tYPE_ITEM == null)
            {
                return HttpNotFound();
            }
            return View(tYPE_ITEM);
        }

        // GET: TYPE_ITEM/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TYPE_ITEM/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TYPE_CODE,TYPE_NAME")] TYPE_ITEM tYPE_ITEM)
        {
            if (ModelState.IsValid)
            {
                db.TYPE_ITEM.Add(tYPE_ITEM);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tYPE_ITEM);
        }

        // GET: TYPE_ITEM/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TYPE_ITEM tYPE_ITEM = db.TYPE_ITEM.Find(id);
            if (tYPE_ITEM == null)
            {
                return HttpNotFound();
            }
            return View(tYPE_ITEM);
        }

        // POST: TYPE_ITEM/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TYPE_CODE,TYPE_NAME")] TYPE_ITEM tYPE_ITEM)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tYPE_ITEM).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tYPE_ITEM);
        }

        // GET: TYPE_ITEM/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TYPE_ITEM tYPE_ITEM = db.TYPE_ITEM.Find(id);
            if (tYPE_ITEM == null)
            {
                return HttpNotFound();
            }
            return View(tYPE_ITEM);
        }

        // POST: TYPE_ITEM/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            TYPE_ITEM tYPE_ITEM = db.TYPE_ITEM.Find(id);
            db.TYPE_ITEM.Remove(tYPE_ITEM);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
